/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
 * @file zwerc.h
 *
 * Parse ELF files
 *
 * @author Trey Boehm
 */
#ifndef __ZWERC_H__
#define __ZWERC_H__
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// Size of an ELF header, in bytes
#define ELF_HEADER_SIZE     52
/// Size of a program header, in bytes
#define PROGRAM_HEADER_SIZE 32
/// Size of a section header, in bytes
#define SECTION_HEADER_SIZE 40
/// ELF magic bytes (Little Endian)
#define ELF_MAGIC           0x464c457f

/**
 * Definitions for offsets into the ELF header
 */
enum ELF_Offsets {
    EI_MAG0       = 0x00, // ELF magic byte 0, 0x7f
    EI_MAG1       = 0x01, // ELF magic byte 1, 'E'
    EI_MAG2       = 0x02, // ELF magic byte 2, 'L'
    EI_MAG3       = 0x03, // ELF magic byte 2, 'F'
    EI_CLASS      = 0x04, // Architecture. 1 for 32-bit, 2 for 64-bit
    EI_DATA       = 0x05, // Byte order. 1 for little endian, 2 for big
    EI_VERSON     = 0x06, // ELF version. 1 for current version
    EI_OSABI      = 0x07, // Target operating system ABI
    EI_ABIVERSION = 0x08, // ABI-dependent
    EI_PAD        = 0x09, // Unused
    EH_TYPE       = 0x10, // Identifies object file type
    EH_MACHINE    = 0x12, // Specifies ISA
    EH_VERSION    = 0x14, // Set to 1 for original version of ELF
    EH_ENTRY      = 0x18, // Entry point for the program
    EH_PHOFF      = 0x1c, // Start of program header table
    EH_SHOFF      = 0x20, // Start of section header table
    EH_FLAGS      = 0x24, // Architecture-dependent
    EH_EHSIZE     = 0x28, // Size of this header (52 for 32-bit)
    EH_PHENTSIZE  = 0x2a, // Size of a program header table entry
    EH_PHNUM      = 0x2c, // Number of entries in program header table
    EH_SHENTSIZE  = 0x2e, // Size of a section header table entry
    EH_SHNUM      = 0x30, // Number of entries in section header table
    EH_SHSTRNDX   = 0x32, // Index of the section header table that
                          // contains the section names
};

/// EI_CLASS value for 32-bit ELF
#define ELF_32              1
/// EI_CLASS value for 64-bit ELF
#define ELF_64              2
/// EI_DATA value for Little Endian
#define ELF_LITTLE_ENDIAN   1
/// EI_DATA value for Big Endian
#define ELF_BIG_ENDIAN      2
/// EI_VERSION value for "current ELF version"
#define ELF_CURRENT_VERSION 1

/**
 * ABI used by the ELF
 */
enum ELF_ABI {
    ABI_SYSTEM_V = 0x00, ///< System-V (default)
    ABI_NETBSD   = 0x02, ///< NetBSD
    ABI_LINUX    = 0x03, ///< Linux
    ABI_GNU_HURD = 0x04, ///< GNU Hurd
    ABI_SOLARIS  = 0x06, ///< Solaris
    ABI_FREEBSD  = 0x09, ///< FreeBSD
    ABI_OPENBSD  = 0x0c, ///< OpenBSD
};

/**
 * ELF type meanings
 */
enum ELF_ET {
    ET_NONE   = 0x00,   ///< Unknown type
    ET_REL    = 0x01,   ///< Relocatable (object) file
    ET_EXEC   = 0x02,   ///< Executable file
    ET_DYN    = 0x03,   ///< Dynamic library
    ET_CORE   = 0x04,   ///< Core file
    ET_LOOS   = 0xfe00, ///< Start OS-specific
    ET_HIOS   = 0xfeff, ///< End OS-specific
    ET_LOPROC = 0xff00, ///< Start processor-specific
    ET_HIPROC = 0xffff, ///< End processor-specific
};

/**
 * ELF instruction set architecture
 */
enum ELF_ISA {
    ISA_NONE    = 0x00, ///< Unknown ISA
    ISA_SPARC   = 0x02, ///< SPARC
    ISA_X86     = 0x03, ///< x86
    ISA_MIPS    = 0x08, ///< MIPS
    ISA_POWERPC = 0x14, ///< PowerPC
    ISA_S390    = 0x16, ///< S390
    ISA_ARM     = 0x28, ///< ARM
    ISA_SUPERH  = 0x2a, ///< SuperH
    ISA_IA_64   = 0x32, ///< IA-64
    ISA_X86_64  = 0x3e, ///< x86-64
    ISA_AARCH64 = 0xb7, ///< AArch64
    ISA_RISC_V  = 0xf3, ///< RISC-V
};

/**
 * Identify the type of program segment
 */
enum PROGRAM_TYPES {
    PT_NULL    = 0x00000000, ///< Program header table entry unused
    PT_LOAD    = 0x00000001, ///< Loadable segment
    PT_DYNAMIC = 0x00000002, ///< Dynamic linking information
    PT_INTERP  = 0x00000003, ///< Interpreter information
    PT_NOTE    = 0x00000004, ///< Auxillary information
    PT_SHLIB   = 0x00000005, ///< reserved
    PT_PHDR    = 0x00000006, ///< Program header table
    PT_LOOS    = 0x60000000, ///< Start of reserved OS range
    PT_HIOS    = 0x6fffffff, ///< End of reserved OS range
    PT_LOPROC  = 0x70000000, ///< Start of reserved processor range
    PT_HIPROC  = 0x7fffffff, ///< End of reserved processor range
};

/**
 * Identify the type of section header
 */
enum SECTION_HEADER_TYPES {
    SHT_NULL          = 0x00, ///< Section header table entry unused
    SHT_PROGBITS      = 0x01, ///< Program data
    SHT_SYMTAB        = 0x02, ///< Symbol table
    SHT_STRTAB        = 0x03, ///< Sting table
    SHT_RELA          = 0x04, ///< Relocation entries with addends
    SHT_HASH          = 0x05, ///< Symbol hash table
    SHT_DYNAMIC       = 0x06, ///< Dynamic linking information
    SHT_NOTE          = 0x07, ///< Notes
    SHT_NOBITS        = 0x08, ///< Program space with no data (bss)
    SHT_REL           = 0x09, ///< Relocation entries, no addends
    SHT_SHLIB         = 0x0a, ///< reserved
    SHT_DYNSYM        = 0x0b, ///< Dynamic linker symbol table
    SHT_INIT_ARRAY    = 0x0e, ///< Array of constructors
    SHT_FINI_ARRAY    = 0x0f, ///< Array of destructors
    SHT_PREINIT_ARRAY = 0x10, ///< Array of pre-constructors
    SHT_GROUP         = 0x11, ///< Section group,
    SHT_SYMTAB_SHNDX  = 0x12, ///< Extended section indices
    SHT_NUM           = 0x13, ///< Number of defined types
    SHT_LOOS    = 0x60000000, ///< Start OS-specific
    SHT_RV_ATTR = 0x70000003, ///< RISC-V Attribute
};

/**
 * Attributes associated with a section
 */
enum SECTION_HEADER_FLAGS {
    SHF_WRITE      = 0x01,       ///< Writable                      (W)
    SHF_ALLOC      = 0x02,       ///< In memory during execution    (A)
    SHF_EXECINSTR  = 0x04,       ///< Executable                    (X)
    SHF_MERGE      = 0x10,       ///< Might be merged               (M)
    SHF_STRINGS    = 0x20,       ///< Contains strings              (S)
    SHF_INFO_LINK  = 0x40,       ///< 'sh_info' contains SHT index  (I)
    SHF_LINK_ORDER = 0x80,       ///< Preserve order after linking  (L)
    SHF_OS_NONCONF = 0x100,      ///< OS non-conforming             (O)
    SHF_GROUP      = 0x200,      ///< Is member of group            (G)
    SHF_TLS        = 0x400,      ///< Holds thread-local data       (T)
    SHF_MASKOS     = 0x0ff00000, ///< OS-specific                   (o)
    SHF_MASKPROC   = 0xf0000000, ///< Processor-specific            (p)
};

/**
 * Single-character encoding for section header flags
 *
 * Index into this with the flag bit (or bits) that's set.
 */
const char SectionHeaderFlags[] = "WAXxMSILOGTxxxxxxxxxoooooooopppp";

/**
 * Key to the single-character encoding for section header flags
 *
 * This is copy/pasted from the GNU readelf output.
 */
const char KeyToFlags[] = "Key to Flags:\n\
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),\n\
  L (link order), O (extra OS processing required), G (group), T (TLS),\n\
  C (compressed), x (unknown), o (OS specific), E (exclude),\n\
  p (processor specific)\n";

/**
 * Fields included in the ELF header
 *
 * Padding is included (ei_pad1, ei_pad2, ei_pad3) so we can fread from
 * the image directly into the struct.
 */
struct ELF_Header {
    uint32_t ei_magic;
    uint8_t  ei_class;
    uint8_t  ei_data;
    uint8_t  ei_version;
    uint8_t  ei_osabi;
    uint8_t  ei_abiversion;
    uint8_t  ei_pad1;
    uint16_t ei_pad2;
    uint32_t ei_pad4;
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint32_t e_entry;
    uint32_t e_phoff;
    uint32_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
};

/**
 * Fields included in a program header
 */
struct Program_Header {
    uint32_t p_type;   ///< Type of segment
    uint32_t p_offset; ///< Offset of the segment in the file
    uint32_t p_vaddr;  ///< Virtual address of the segment in memory
    uint32_t p_paddr;  ///< Segment's physical address, if relevant
    uint32_t p_filesz; ///< Size in bytes of the segment in the f
    uint32_t p_memsz;  ///< Size in bytes of the segment in memory
    uint32_t p_flags;  ///< Segment-dependent flags
    uint32_t p_align;  ///< Alignment (power of 2, or 0/1 for none)
};

/**
 * Fields included in a section header
 */
struct Section_Header {
    uint32_t sh_name;      ///< Offset to a string in .shstrtab
    uint32_t sh_type;      ///< Type
    uint32_t sh_flags;     ///< Attributes
    uint32_t sh_addr;      ///< Virtual address of section
    uint32_t sh_offset;    ///< Offset of section in the file
    uint32_t sh_size;      ///< Size of section
    uint32_t sh_link;      ///< Section index of an associated section
    uint32_t sh_info;      ///< Extra information
    uint32_t sh_addralign; ///< Alignment (must be a power of 2)
    uint32_t sh_entsize;   ///< Size of each entry, or 0
};

/**
 * Read in an ELF header from a file
 */
void read_elf_header(
    FILE *image,                  ///< File to read from
    struct ELF_Header *elf_header ///< Struct to read into
);

/**
 * Print out the contents of an ELF header
 */
void print_elf_header(
    struct ELF_Header *elf_header ///< Header to print
);

/**
 * Read in a section header from a file
 */
void read_section_header(
    FILE *image,                     ///< File to read from
    struct ELF_Header *elf_header,   ///< ELF header, for indices
    struct Section_Header *s_header, ///< Section header to read into
    uint16_t shnum                   ///< Section number to read
);

/**
 * Print out the contents of a section header
 */
void print_section_header(
    FILE *image,                     ///< File to read from
    struct Section_Header *s_header, ///< Section header to print
    uint16_t shnum                   ///< Section number to print
);

/**
 * Read in a program header from a file
 */
void read_program_header(
    FILE *image,                     ///< File to read from
    struct ELF_Header *elf_header,   ///< ELF header, for indices
    struct Program_Header *p_header, ///< Program header to read into
    uint16_t phnum                   ///< Program number to read
);

/**
 * Read a section header name
 */
char *read_sh_name(
    FILE *image, ///< File to read from
    uint32_t off ///< Offset into the .shstrtab section
);

/**
 * Read a null-terminated string from a file
 */
char *read_string(
    FILE *image, ///< File to read from
    uint32_t off ///< Offset into the file
);

/**
 * Display an error message
 */
void elf_error(
    char *msg ///< Error message to display
);

#endif /* __ZWERC_H__ */
