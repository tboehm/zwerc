#include "zwerc.h"

static uint32_t shstr_section_offset;

int main(int argc, char **argv)
{
    char *filename;
    FILE *image;
    uint16_t phnum;
    uint16_t snum;

    if (argc == 1) {
        fprintf(stderr, "Specify an input file\n");
        return -1;
    }

    filename = argv[1];
    image = fopen(filename, "r");

    struct ELF_Header elf_header;
    read_elf_header(image, &elf_header);
    print_elf_header(&elf_header);

    // Get the offset for section header strings
    struct Section_Header s_header;
    read_section_header(image, &elf_header, &s_header,
            elf_header.e_shstrndx);
    shstr_section_offset = s_header.sh_offset;

    // Display the section headers
    printf("Section Headers:\n");
    printf("  [Nr] Name              Type            Addr     Off    "
           "Size   ES Flg Lk Inf Al\n");
    for (snum = 0; snum < elf_header.e_shnum; snum++) {
        read_section_header(image, &elf_header, &s_header, snum);
        print_section_header(image, &s_header, snum);
    }
    printf(KeyToFlags);

    struct Program_Header p_header;
    for (phnum = 0; phnum < elf_header.e_phnum; phnum++) {
        read_program_header(image, &elf_header, &p_header, phnum);
    }

    return 0;
}

void read_elf_header(FILE *image, struct ELF_Header *elf_header)
{
    assert(ELF_HEADER_SIZE == sizeof(struct ELF_Header));
    fread(elf_header, sizeof(uint8_t), ELF_HEADER_SIZE, image);

    if (elf_header->ei_magic != ELF_MAGIC) {
        elf_error("Not an ELF file - it has the wrong "
                  "magic bytes at the start");
    }
}

void print_elf_header(struct ELF_Header *elf_header)
{
    printf("ELF Header:\n");
    printf("  Magic:   ");
    for (int i = 0; i < 16; i++) {
        printf("%.2x ", *(((uint8_t *)elf_header) + i));
    }
    putchar('\n');

    printf("  Class:                             ");
    switch (elf_header->ei_class) {
        case ELF_32:
            printf("ELF32\n");
            break;
        case ELF_64:
            printf("ELF64\n");
            break;
        default:
            putchar('\n');
            elf_error("ei_class not valid");
    }

    printf("  Data:                              ");
    switch (elf_header->ei_data) {
        case ELF_LITTLE_ENDIAN:
            printf("2's complement, little endian\n");
            break;
        case ELF_BIG_ENDIAN:
            printf("2's complement, big endian\n");
            break;
        default:
            putchar('\n');
            elf_error("ei_data not valid");
    }

    printf("  Version:                           ");
    switch (elf_header->ei_version) {
        case ELF_CURRENT_VERSION:
            printf("1 (current)\n");
            break;
        default:
            putchar('\n');
            elf_error("ei_version not valid");
    }

    printf("  OS/ABI:                            ");
    switch (elf_header->ei_osabi) {
        case ABI_SYSTEM_V:
            printf("UNIX - System V\n");
            break;
        case ABI_NETBSD:
            printf("NetBSD\n");
            break;
        case ABI_LINUX:
            printf("Linux\n");
            break;
        case ABI_GNU_HURD:
            printf("GNU Hurd\n");
            break;
        case ABI_SOLARIS:
            printf("Solaris\n");
            break;
        case ABI_FREEBSD:
            printf("FreeBSD\n");
            break;
        case ABI_OPENBSD:
            printf("OpenBSD\n");
            break;
        default:
            putchar('\n');
            elf_error("ei_osabi not supported");
    }

    printf("  ABI Version:                       %d\n",
            elf_header->ei_abiversion);

    printf("  Type:                              ");
    switch (elf_header->e_type) {
        case ET_NONE:
            printf("Unknown\n");
            break;
        case ET_REL:
            printf("REL (Relocatable file)\n");
            break;
        case ET_EXEC:
            printf("EXEC (Executable file)\n");
            break;
        case ET_DYN:
            printf("DYN (Shared object)\n");
            break;
        case ET_CORE:
            printf("CORE (Core file)\n");
            break;
        case ET_LOOS:
            printf("LOOS\n");
            break;
        case ET_HIOS:
            printf("HIOS\n");
            break;
        case ET_LOPROC:
            printf("LOPROC\n");
            break;
        case ET_HIPROC:
            printf("HIPROC\n");
            break;
        default:
            putchar('\n');
            elf_error("e_type not supported");
    }

    printf("  Machine:                           ");
    switch (elf_header->e_machine) {
        case ISA_NONE:
            printf("None\n");
            break;
        case ISA_SPARC:
            printf("SPARC\n");
            break;
        case ISA_X86:
            printf("x86\n");
            break;
        case ISA_MIPS:
            printf("MIPS\n");
            break;
        case ISA_POWERPC:
            printf("PowerPC\n");
            break;
        case ISA_S390:
            printf("S390\n");
            break;
        case ISA_ARM:
            printf("ARM\n");
            break;
        case ISA_SUPERH:
            printf("SuperH\n");
            break;
        case ISA_IA_64:
            printf("IA-64\n");
            break;
        case ISA_X86_64:
            printf("x86-64\n");
            break;
        case ISA_AARCH64:
            printf("AArch64\n");
            break;
        case ISA_RISC_V:
            printf("RISC-V\n");
            break;
        default:
            putchar('\n');
            elf_error("e_machine unknown");
    }

    printf("  Version:                           0x%.x\n",
            elf_header->e_version);
    printf("  Entry point address:               0x%.8x\n",
            elf_header->e_entry);
    printf("  Start of program headers:          %d (bytes into file)\n",
            elf_header->e_phoff);
    printf("  Start of section headers:          %d (bytes into file)\n",
            elf_header->e_shoff);
    printf("  Flags:                             0x%x\n",
            elf_header->e_flags);
    printf("  Size of this header:               %d (bytes)\n",
            elf_header->e_ehsize);
    printf("  Size of program headers:           %d (bytes)\n",
            elf_header->e_phentsize);
    printf("  Number of program headers:         %d\n",
            elf_header->e_phnum);
    printf("  Size of section headers:           %d (bytes)\n",
            elf_header->e_shentsize);
    printf("  Number of section headers:         %d\n",
            elf_header->e_shnum);
    printf("  Section header string table index: %d\n\n",
            elf_header->e_shstrndx);
}

void read_section_header(FILE *image, struct ELF_Header *elf_header,
        struct Section_Header *s_header, uint16_t shnum)
{
    uint32_t pos;
    assert(SECTION_HEADER_SIZE == sizeof(struct Section_Header));

    pos = elf_header->e_shoff + (shnum * elf_header->e_shentsize);
    fseek(image, pos, SEEK_SET);
    fread(s_header, sizeof(uint8_t), SECTION_HEADER_SIZE, image);
}

void print_section_header(FILE *image, struct Section_Header *s_header,
        uint16_t shnum)
{
    char *section_name; // read from shstrtab
    uint32_t bit;       // flag parsing
    uint32_t num_flags; // flag printing
    char flags[6];      // flag printing

    printf("  [ %d] ", shnum);
    section_name = read_sh_name(image, s_header->sh_name);
    if (section_name != NULL) {
        printf("%-18s", section_name);
        free(section_name);
    } else {
        printf("%-18s", " ");
    }
    switch (s_header->sh_type) {
        case SHT_NULL:
            printf("NULL            ");
            break;
        case SHT_PROGBITS:
            printf("PROGBITS        ");
            break;
        case SHT_SYMTAB:
            printf("SYMTAB          ");
            break;
        case SHT_STRTAB:
            printf("STRTAB          ");
            break;
        case SHT_RELA:
            printf("RELA            ");
            break;
        case SHT_HASH:
            printf("HASH            ");
            break;
        case SHT_DYNAMIC:
            printf("DYNAMIC         ");
            break;
        case SHT_NOTE:
            printf("NOTE            ");
            break;
        case SHT_NOBITS:
            printf("NOBITS          ");
            break;
        case SHT_REL:
            printf("REL             ");
            break;
        case SHT_SHLIB:
            printf("SHLIB           ");
            break;
        case SHT_DYNSYM:
            printf("DYNSYM          ");
            break;
        case SHT_INIT_ARRAY:
            printf("INIT_ARRAY      ");
            break;
        case SHT_FINI_ARRAY:
            printf("FINI_ARRAY      ");
            break;
        case SHT_PREINIT_ARRAY:
            printf("PREINIT_ARRA Y  ");
            break;
        case SHT_GROUP:
            printf("GROUP           ");
            break;
        case SHT_SYMTAB_SHNDX:
            printf("SYMTAB_SHNDX    ");
            break;
        case SHT_RV_ATTR:
            printf("RISCV_ATTRIBUTE ");
            break;
        default:
            putchar('\n');
            elf_error("sh_type invalid");
    }
    printf("%.8x ", s_header->sh_addr);
    printf("%.6x ", s_header->sh_offset);
    printf("%.6x ", s_header->sh_size);
    printf("%.2x ", s_header->sh_entsize);

    num_flags = 0;
    for (bit = 0; bit < 32; bit++) {
        if (s_header->sh_flags & (1 << bit)) {
            flags[num_flags] = SectionHeaderFlags[bit];
            num_flags++;
        }
    }
    flags[num_flags] = '\0';
    printf("%3s ", flags);

    printf("%2d ", s_header->sh_link);
    printf("%3d ", s_header->sh_info);
    printf("%2d", s_header->sh_addralign);
    putchar('\n');
}

void read_program_header(FILE *image, struct ELF_Header *elf_header,
        struct Program_Header *p_header, uint16_t phnum)
{
    uint32_t pos;
    assert(PROGRAM_HEADER_SIZE == sizeof(struct Program_Header));

    pos = elf_header->e_phoff + (phnum * elf_header->e_phentsize);
    fseek(image, pos, SEEK_SET);
    fread(p_header, sizeof(uint8_t), PROGRAM_HEADER_SIZE, image);
}

char *read_sh_name(FILE *image, uint32_t sh_name)
{
    uint32_t off;
    char *str;

    off = shstr_section_offset + sh_name;
    str = read_string(image, off);

    return str;
}

char *read_string(FILE *image, uint32_t off)
{
    uint32_t len;
    char c;
    char *str;

    fseek(image, off, SEEK_SET);
    len = 0;
    for (len = 0; ((c = fgetc(image)) != '\0'); len++) { ; }

    if (len == 0) {
        return NULL;
    }

    str = (char *)malloc(sizeof(char) * (len + 1));
    if (str == NULL) {
        return NULL;
    }

    fseek(image, off, SEEK_SET);
    if (fread(str, len, 1, image) != 1) {
        return NULL;
    }

    str[len] = '\0';
    return str;
}

void elf_error(char *msg)
{
    fprintf(stderr, "zwerc: Error: %s\n", msg);
    exit(EXIT_FAILURE);
}
