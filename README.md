zwerc
=====

Parse ELF files and generate output that matches the GNU readelf tool.

Why?
----

In order to get [RISC-V compliance tests](https://github.com/riscv/riscv-compliance/) to run in my [simulator](https://gitlab.com/tboehm/riscv), I need to be able to read in ELF files (not just hex files). Before starting this, I did not know a lot about the specification. Matching the output of GNU readelf exactly is not at all necessary, but doing so makes it easy to compare my output to something stable. Ultimately, chunks of this code will be included in my simulator to read in relevant sections.

Dependencies
------------
The Makefile assumes you have a RISC-V assembly program that you want to assemble into an ELF and view. You'll need the [GNU toolchain](https://github.com/riscv/riscv-gnu-toolchain/) for the assembly. I have a target, ``nav``, which uses cscope and ctags to make files for vim navigation and syntax highlighting. Remove that if you don't want it and the program will be unaffected.
