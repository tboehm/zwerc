addi x1, x0, 5   # x1 = 0x00000005
addi x2, x1, 8   # x2 = 0x0000000d
addi x3, x2, 12  # x3 = 0x00000019
addi x4, x3, -12 # x4 = 0x0000000d
addi x5, x4, -12 # x5 = 0x00000001
addi x6, x5, -12 # x6 = 0xfffffff5
