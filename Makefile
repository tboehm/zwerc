# Copyright (c) 2019 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

PROGRAM=zwerc
PROJ_NAME=$(PROGRAM)

# Compiler and flags
CC=gcc
WFLAGS=-Wall -Wextra
CFLAGS=$(WFLAGS)
SRC=src
SRC_FILES=$(shell ls $(SRC)/*.c)
HDR_FILES=$(shell ls $(SRC)/*.h)

# RISC-V toolchain
RV=riscv32-unknown-elf
RV_OPTS=-march=rv32i -static -mcmodel=medany -fvisibility=hidden -nostdlib -nostartfiles -T ~/etc/riscv-compliance/riscv-test-env/p/link.ld
RV_CC=$(RV)-gcc
RV_READELF=$(RV)-readelf

# Navigational tools
CTAGS=/opt/local/bin/ctags
CSCOPE=cscope

# Put documentation in a directory called $(DOCS). HTML and LaTeX
# documentation each go in separate dirs that are set in the Doxygen
# configuration file, $(DOCS)/$(DOXYFILE)
DOCS=docs
DOXYFILE=$(DOCS)/Doxyfile
DOXYGEN_OPTS = echo 'PROJECT_NAME = "$(PROJ_NAME)"';\
		echo 'PROJECT_BRIEF = "Parse ELF files"';\
		echo 'INPUT = .'

# Input file
ASMFILE=test/addi.s
ELFFILE=$(ASMFILE:.s=.elf)

.PHONY: all
all: nav test 

.PHONY: test
test: $(PROGRAM) $(ELFFILE)
	./$(PROGRAM) $(ELFFILE)

$(PROGRAM): $(SRC_FILES) $(HDR_FILES)
	$(CC) $(CFLAGS) -o $@ $<

$(ELFFILE): $(ASMFILE)
	$(RV_CC) $(RV_OPTS) $< -o $@

# Files for navigational tools ctags and cscope
.PHONY: nav
nav: tags cscope.out

# Tags file for ctags.
tags: $(SRC_FILES) $(HDR_FILES)
	@printf "  Generating tags file\n"
	@$(CTAGS) -f tags -R .

# Database for cscope.
cscope.out: $(SRC_FILES) $(HDR_FILES)
	@printf "  Generating cscope database\n"
	@$(CSCOPE) -b $^

# All (both) types of documentation.
.PHONY: docs
docs: $(DOXYFILE) $(LIB_HDRS) $(LOCAL_HDRS)
	@printf "  Creating documentation\n"
	@( cat $(DOXYFILE);\
		$(DOXYGEN_OPTS);\
	 ) | doxygen -

# HTML documentation. I want to be able to control whether I am building
# only HTML, only LaTeX, or both, so I have both set to "YES" by default
# in the $(DOXYFILE). To disable one, I just cat the file, append the
# corresponding "GENERATE_x=NO", and have Doxygen read from stdin
# instead of a file directly. The $(PROJ_NAME) is set in the local
# Makefile for each project.
docs/html: $(DOXYFILE) $(HDR_FILES)
	@printf "  Generating HTML documentation\n"
	@( cat $(DOXYFILE);\
		echo 'GENERATE_LATEX=NO';\
		$(DOXYGEN_OPTS);\
	 ) | doxygen -

# PDF (LaTeX) documentation. This target is nearly identical to the HTML
# target above. Instead of disable LaTeX generation, though, it disables
# HTML generation.
docs/latex: $(DOXYFILE) $(HDR_FILES)
	@printf "  Generating LaTeX documentation\n"
	@( cat $(DOXYFILE);\
		echo 'GENERATE_HTML=NO';\
		$(DOXYGEN_OPTS);\
	 ) | doxygen -

# Clean just removes the binary
.PHONY: clean
clean:
	@rm -f $(PROGRAM)

# Distclean gets rid of everything else
.PHONY: distclean
distclean: clean
	@rm -f ctags tags types_*.taghl cscope.out
	@rm -rf $(DOCS)/html $(DOCS)/latex
